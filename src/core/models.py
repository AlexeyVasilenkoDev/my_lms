from datetime import date
from uuid import uuid4

from django.contrib.auth import get_user_model
from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.utils import timezone
from django.utils.safestring import mark_safe
from location_field.models.plain import PlainLocationField
from phonenumber_field.modelfields import PhoneNumberField

from django.db import models  # NOQA

from django.utils.translation import gettext_lazy as _

from core.managers import CustomerManager


class CustomUser(AbstractBaseUser, PermissionsMixin):
    first_name = models.CharField(_("name"), max_length=150, blank=True)
    last_name = models.CharField(_("surname"), max_length=150, blank=True)
    email = models.EmailField(_("email address"))
    phone = PhoneNumberField(unique=True)
    is_staff = models.BooleanField(
        _("staff status"),
        default=False,
        help_text=_("Designates whether the user can log into this admin site."),
    )
    is_active = models.BooleanField(
        _("active"),
        default=True,
        help_text=_(
            "Designates whether this user should be treated as active. " "Unselect this instead of deleting accounts."
        ),
    )
    date_joined = models.DateTimeField(_("date joined"), default=timezone.now)
    photo = models.ImageField(default="img/Photo.jpg", upload_to="img/")

    objects = CustomerManager()

    USERNAME_FIELD = "phone"
    REQUIRED_FIELDS = []

    class Meta:
        verbose_name = _("user")
        verbose_name_plural = _("users")

    def clean(self):
        super().clean()
        self.email = self.__class__.objects.normalize_email(self.email)

    def get_full_name(self):
        """
        Return the first_name plus the last_name, with a space in between.
        """
        full_name = "%s %s" % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        """Return the short name for the user."""
        return self.first_name

    def __str__(self):
        return str(self.phone)


class Profile(models.Model):
    STUDENT = "Student"
    TEACHER = "Teacher"
    MENTOR = "Mentor"
    USER_TYPE_CHOICES = (
        (STUDENT, "Student"),
        (TEACHER, "Teacher"),
        (MENTOR, "Mentor"),
    )
    uuid = models.UUIDField(
        primary_key=True,
        default=uuid4,
        unique=True,
        db_index=True,
        editable=False,
    )
    user = models.OneToOneField(get_user_model(), on_delete=models.CASCADE, null=True)
    phone_number = PhoneNumberField(null=True, blank=True)
    photo = models.ImageField(
        default="img/Photo.jpg",
        upload_to="img/",
    )
    location = PlainLocationField(based_fields=['city'], zoom=7, null=True, blank=True,
                                  default='50.45015108024268,30.523978471755978')
    user_type = models.CharField(max_length=50, choices=USER_TYPE_CHOICES, blank=True)

    def __str__(self):
        return f"{self.user.phone}"


class Person(models.Model):
    uuid = models.UUIDField(
        primary_key=True,
        default=uuid4,
        unique=True,
        db_index=True,
        editable=False,
    )
    photo = models.ImageField(
        default="img/Photo.jpg",
        upload_to="img/",
    )
    first_name = models.CharField(
        max_length=100,
        blank=True,
        default="",
    )
    last_name = models.CharField(
        max_length=100,
        blank=True,
        default="",
    )
    email = models.EmailField(
        max_length=100,
        null=True,
        default=None,
    )
    birth_date = models.DateField(
        null=True,
        default=None,
    )

    def admin_photo(self):
        return mark_safe(f"<img src='{self.photo.url}' width='100vh'")
    admin_photo.short_description = "Photo"
    admin_photo.allow_tags = True

    def __str__(self):
        return f"{self.first_name} {self.last_name}: {self.email}"

    def age(self):
        today = date.today()
        current_age = (
                today.year # NOQA
                - self.birth_date.year
                - ((today.month, today.day) < (self.birth_date.month, self.birth_date.day))
        )
        return current_age

    class Meta:
        abstract = True
