from django.urls import path
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

from core.views import IndexView, Login, Logout, Registration, ActivateUser, ProfileView

app_name = "core"

urlpatterns = [
    path("", IndexView.as_view(), name="core"),
    path("login/", Login.as_view(), name="login"),
    path("logout/", Logout.as_view(), name="logout"),
    path("registration/", Registration.as_view(), name="registration"),
    path("activate/<str:uuid64>/<str:token>", ActivateUser.as_view(), name="activation"),
    path("profile/", ProfileView.as_view(), name="profile"),
]

urlpatterns += staticfiles_urlpatterns()
