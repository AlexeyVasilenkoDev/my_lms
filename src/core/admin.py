from django.contrib import admin  # NOQA

from core.models import Profile, CustomUser

# Register your models here.
from groups.models import Group
from students.models import Student
from teachers.models import Teacher

admin.site.register([Profile, CustomUser])


@admin.register(Student)
class StudentAdmin(admin.ModelAdmin):
    list_display = ["uuid", "admin_photo", "first_name", "last_name", "email", "birth_date", "age", "group", "grade",
                    "campus", "active", "cv"]
    list_filter = ("campus", "active")
    search_fields = ("first_name", "last_name")
    search_help_text = "Search by first or last name"
    date_hierarchy = "birth_date"

    fieldsets = (
        (
            "Main information", {
                "fields": (("first_name", "last_name"), "email", "group")
            }
        ),
        (
            "Additional information", {
                "classes": ("collapse",),
                "fields": ("birth_date", "photo", "grade", "cv", "campus", "active",)
            }
        )
    )


@admin.register(Teacher)
class TeacherAdmin(admin.ModelAdmin):
    list_display = ["uuid", "admin_photo", "first_name", "last_name", "email", "birth_date", "age", "salary",
                    "group_count", "salary_per_course"]
    list_filter = ("email",)
    search_fields = ("first_name", "last_name")
    search_help_text = "Search by first or last name"

    filter_horizontal = ("group", )
    fieldsets = (
        (
            "Main information", {
                "fields": (("first_name", "last_name"), "email")
            }
        ),
        (
            "Additional information", {
                "classes": ("collapse",),
                "fields": ("birth_date", "photo", "salary", "group")
            }
        )
    )

    def group_count(self, obj):
        return obj.group.count()

    def salary_per_course(self, obj):
        return obj.salary / self.group_count(obj)


@admin.register(Group)
class GroupAdmin(admin.ModelAdmin):
    list_display = ("course", "length_in_months", "price", "number_of_students")
    list_filter = ("lesson_duration", "price")
    search_fields = ("course",)
    search_help_text = "Search by course"

    fieldsets = (
        (
            "Main information", {
                "fields": ("course", "length_in_months", "price")
            }
        ),
        (
            "Additional information", {
                "classes": ("collapse",),
                "fields": ("number_of_students", "lesson_duration", "website")
            }
        )
    )
