import os

from config.settings import BASE_DIR


def get_file_path(instance, filename):
    if os.path.exists(BASE_DIR / "media" / "img" + filename):
        return os.path.normpath("img/" + filename)
    return "img/" + filename
