from django.contrib.auth import login, get_user_model
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from django.contrib.auth.views import LogoutView, LoginView
from django.http import HttpResponse
from django.shortcuts import render
from django.urls import reverse_lazy
from django.utils.encoding import force_str
from django.utils.http import urlsafe_base64_decode
from django.views.generic import TemplateView, CreateView, RedirectView

from config import settings
from core.forms import RegistrationForm
from services.emails import send_registration_email
from utils.token_generator import TokenGenerator


class IndexView(TemplateView):
    template_name = "index/index.html"


class NotFoundView(TemplateView):
    template_name = "index/404.html"


class Login(LoginView):
    pass


class Logout(LogoutView):
    pass


class Registration(CreateView):
    form_class = RegistrationForm
    template_name = "registration/user_form.html"
    success_url = reverse_lazy("core:core")

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.is_active = False
        self.object.save()

        send_registration_email(
            request=self.request, user_instance=self.object
        )
        return super().form_valid(form)


class ActivateUser(RedirectView):
    url = reverse_lazy("core:core")

    def get(self, request, uuid64, token, *args, **kwargs):
        try:
            pk = force_str(urlsafe_base64_decode(uuid64))
            user = get_user_model().objects.get(pk=pk)
        except (get_user_model().DoesNotExists, TypeError, ValueError):
            return HttpResponse('Error')
        if user and TokenGenerator().check_token(user, token):
            user.is_active = True
            user.save()

            login(request, user, backend='django.contrib.auth.backends.ModelBackend')
            return super().get(request, *args, **kwargs)
        return render(request, "emails/activation_error.html")


class ProfileView(LoginRequiredMixin, TemplateView):
    model = User
    template_name = "profiles/profile.html"

    def get(self, request, *args, **kwargs):
        self.extra_context = {"key_api": settings.LOCATION_FIELD['provider.google.api_key']}

        return self.render_to_response(self.extra_context)

# def email(request):
#     send_mail(
#         subject="Test message",
#         message=f"Homework done by Vasylenko Olexiy at {datetime.datetime.now()}!",
#         from_email=EMAIL_HOST_USER,
#         recipient_list=["aleksejvasilenko0@gmail.com"],
#     )
#     return HttpResponse("Email sent!")
