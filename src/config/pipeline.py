# import os.path
# import pprint
# import requests
# import shutil
#
# from django.db import models
# from django.db.models import Model

from core.models import Profile


def set_profile(backend, uid, user=None, *args, **kwargs):
    try:
        profile = Profile.objects.get(user=user)
    except Profile.DoesNotExist:
        profile = Profile.objects.create(user=user)
    # if user.provider == "google-oauth2":
    #     user.profile.photo = f'{kwargs["response"]["picture"]}'
    #
    #     res = requests.get(kwargs["response"]["picture"], stream=True)
    #
    #     if os.path.exists(f"media/img/{user.username}_photo.png"):
    #         pass
    #     else:
    #         with open(f"media/img/{user.username}_photo.png", 'wb') as f:
    #             shutil.copyfileobj(res.raw, f)
    #
    #     setattr(profile, "photo", f"img/{user.username}_photo.png")

    user.save()
    profile.save()
