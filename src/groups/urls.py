from django.urls import path

from groups.views import GroupView, CreateGroupView, UpdateGroupView, DeleteGroupView, ShowGroupView

app_name = "groups"

urlpatterns = [
    path("", GroupView.as_view(), name="groups"),
    path("create/", CreateGroupView.as_view(), name="create_group"),
    path("update/<uuid:pk>/", UpdateGroupView.as_view(), name="update_group"),
    path("delete/<uuid:pk>/", DeleteGroupView.as_view(), name="delete_group"),
    path("<course>/", ShowGroupView.as_view(), name="show_group"),
]
