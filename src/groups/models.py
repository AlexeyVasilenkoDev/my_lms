from uuid import uuid4

from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator
from faker import Faker


class Group(models.Model):
    uuid = models.UUIDField(
        primary_key=True,
        default=uuid4,
        unique=True,
        db_index=True,
        editable=False,
    )
    course = models.CharField(
        max_length=100,
        blank=True,
        default="",
    )
    length_in_months = models.PositiveSmallIntegerField(
        null=True,
        validators=[MinValueValidator(1), MaxValueValidator(12)],
    )
    price = models.PositiveIntegerField(
        null=True,
        validators=[MinValueValidator(1000), MaxValueValidator(50000)],
    )
    number_of_students = models.PositiveSmallIntegerField(
        null=True,
        validators=[MinValueValidator(3), MaxValueValidator(30)],
    )
    lesson_duration = models.PositiveSmallIntegerField(
        null=True,
        validators=[MinValueValidator(1), MaxValueValidator(5)],
    )
    website = models.URLField(
        null=True,
    )

    def __str__(self):
        return f"{self.course}"

    @classmethod
    def generate_instances(cls, count):
        faker = Faker()

        for _ in range(count):
            cls.objects.create(
                course=cls.course,
                length_in_months=faker.random.randint(1, 12),
                price=faker.random.randint(1000, 50000),
                number_of_students=faker.random.randint(3, 30),
                lesson_duration=faker.random.randint(1, 5),
                website="https://" + faker.domain_name(),
            )

    class Meta:
        ordering = ("course", )
