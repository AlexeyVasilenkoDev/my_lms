from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.views.generic import CreateView, DeleteView, UpdateView, TemplateView

from groups.forms import GroupForm
from groups.models import Group


class GroupView(LoginRequiredMixin, TemplateView):
    model = Group
    template_name = "groups/groups.html"

    def get(self, request, *args, **kwargs):
        search_query = request.GET.get("q")
        if search_query:
            groups = Group.objects.filter(course__contains=search_query)
        else:
            groups = Group.objects.all()
        self.extra_context = {"groups": groups}

        return self.render_to_response(self.extra_context)


class CreateGroupView(LoginRequiredMixin, CreateView):
    model = Group
    form_class = GroupForm
    template_name = "groups/groups_create.html"
    success_url = reverse_lazy("groups:groups")


class UpdateGroupView(LoginRequiredMixin, UpdateView):
    model = Group
    form_class = GroupForm
    template_name = "groups/groups_update.html"
    success_url = reverse_lazy("groups:groups")


class DeleteGroupView(LoginRequiredMixin, DeleteView):
    model = Group
    success_url = reverse_lazy("groups:groups")
    template_name = "groups/groups_delete.html"


class ShowGroupView(LoginRequiredMixin, TemplateView):
    model = Group
    template_name = "groups/show_group.html"

    def get(self, request, *args, **kwargs):
        groups = Group.objects.filter(course=kwargs["course"])
        self.extra_context = {"groups": groups}

        return self.render_to_response(self.extra_context)
