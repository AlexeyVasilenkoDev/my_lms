import random

from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator
from faker import Faker

from groups.models import Group
from core.models import Person


class Student(Person):
    grade = models.PositiveSmallIntegerField(
        null=True,
        validators=[MinValueValidator(1), MaxValueValidator(100)],
    )
    campus = models.BooleanField(
        default=True,
    )
    active = models.BooleanField(
        default=True,
    )
    group = models.ForeignKey(
        to="groups.Group",
        on_delete=models.CASCADE,
    )
    cv = models.FileField(
        default="cv/bartender-cv-template-in-word-and-pdf-format-for.pdf",
        upload_to="cv/",
    )

    @classmethod
    def generate_instances(cls, count):
        faker = Faker()

        for _ in range(count):
            cls.objects.create(
                first_name=faker.first_name(),
                last_name=faker.last_name(),
                email=faker.email(),
                grade=faker.random.randint(1, 100),
                birth_date=faker.date_time_between(start_date="-30y", end_date="-18y"),
                campus=faker.boolean(),
                active=faker.boolean(),
                group=random.choice(Group.objects.all()),
            )

    class Meta:
        ordering = ("first_name", "last_name")
