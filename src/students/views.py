from django.db.models import Q
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import CreateView, UpdateView, DeleteView, TemplateView
from django.contrib.auth.mixins import LoginRequiredMixin

from students.forms import StudentForm
from students.models import Student


class StudentView(LoginRequiredMixin, TemplateView):
    model = Student
    template_name = "students/students.html"

    def get(self, request, *args, **kwargs):
        search_query = request.GET.get("q")
        if search_query:
            students = Student.objects.filter(
                Q(first_name__contains=search_query) | Q(last_name__contains=search_query)
            )
        else:
            students = Student.objects.all()
        self.extra_context = {"students": students}

        return self.render_to_response(self.extra_context)


class CreateStudentView(LoginRequiredMixin, CreateView):
    model = Student
    form_class = StudentForm
    template_name = "students/students_create.html"
    success_url = reverse_lazy("students:students")


class UpdateStudentView(LoginRequiredMixin, UpdateView):
    model = Student
    form_class = StudentForm
    template_name = "students/students_update.html"
    success_url = reverse_lazy("students:students")


class DeleteStudentView(LoginRequiredMixin, DeleteView):
    model = Student
    success_url = reverse_lazy("students:students")
    template_name = "students/students_delete.html"


def show_students(request, group):
    return render(
        request, template_name="students/show_students.html", context={"students": Student.objects.filter(group=group)}
    )


class ShowStudentsView(LoginRequiredMixin, TemplateView):
    model = Student
    template_name = "students/show_students.html"

    def get(self, request, *args, **kwargs):
        students = Student.objects.filter(group=kwargs["group"])
        self.extra_context = {"students": students}

        return self.render_to_response(self.extra_context)
