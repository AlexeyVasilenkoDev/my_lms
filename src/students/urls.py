from django.urls import path

from students.views import StudentView, CreateStudentView, UpdateStudentView, DeleteStudentView, ShowStudentsView

app_name = "students"

urlpatterns = [
    path("", StudentView.as_view(), name="students"),
    path("create/", CreateStudentView.as_view(), name="create_student"),
    path("update/<uuid:pk>/", UpdateStudentView.as_view(), name="update_student"),
    path("delete/<uuid:pk>/", DeleteStudentView.as_view(), name="delete_student"),
    path("<group>/", ShowStudentsView.as_view(), name="show_students"),
]
