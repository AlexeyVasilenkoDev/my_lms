# Generated by Django 4.1 on 2022-08-22 13:32

import django.core.validators
from django.db import migrations, models
import django.db.models.deletion
import uuid


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ("groups", "0001_initial"),
    ]

    operations = [
        migrations.CreateModel(
            name="Student",
            fields=[
                (
                    "uuid",
                    models.UUIDField(
                        db_index=True,
                        default=uuid.uuid4,
                        editable=False,
                        primary_key=True,
                        serialize=False,
                        unique=True,
                    ),
                ),
                ("photo", models.ImageField(default="img/Photo.jpg", upload_to="img/")),
                (
                    "first_name",
                    models.CharField(blank=True, default="", max_length=100),
                ),
                ("last_name", models.CharField(blank=True, default="", max_length=100)),
                ("email", models.EmailField(default=None, max_length=100, null=True)),
                ("birth_date", models.DateField(default=None, null=True)),
                (
                    "grade",
                    models.PositiveSmallIntegerField(
                        null=True,
                        validators=[
                            django.core.validators.MinValueValidator(1),
                            django.core.validators.MaxValueValidator(100),
                        ],
                    ),
                ),
                ("campus", models.BooleanField(default=True)),
                ("active", models.BooleanField(default=True)),
                (
                    "cv",
                    models.FileField(
                        default="cv/bartender-cv-template-in-word-and-pdf-format-for.pdf",
                        upload_to="cv/",
                    ),
                ),
                (
                    "group",
                    models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to="groups.group"),
                ),
            ],
            options={
                "abstract": False,
            },
        ),
    ]
