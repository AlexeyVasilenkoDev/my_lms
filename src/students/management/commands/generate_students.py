from django.core.management.base import BaseCommand

import csv
import pprint

from faker import Faker

fake = Faker()


class Command(BaseCommand):
    help = "Generating students"

    def add_arguments(self, parser):
        parser.add_argument("number", type=int, help="The amount of students to be generated")

    def generate_students(self, count):
        fieldnames = ["Name", "Email", "Password", "Birthday"]
        students_list = [
            {
                "Name": fake.name(),
                "Email": fake.profile("mail")["mail"],
                "Password": fake.password(length=8),
                "Birthday": str(fake.date_of_birth(minimum_age=18, maximum_age=45)),
            }
            for _ in range(count)
        ]
        with open(
            "new_students.csv",
            "w",
            newline="",
        ) as my_csv:
            writer = csv.DictWriter(my_csv, fieldnames=fieldnames)
            writer.writeheader()
            writer.writerows(student for student in students_list)
        pprint.pprint(students_list, sort_dicts=False)

    def handle(self, *args, **options):
        number = options["number"]
        self.generate_students(number)
