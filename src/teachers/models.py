import random

from django.db import models
from django.core.validators import MinValueValidator
from faker import Faker

from groups.models import Group
from core.models import Person


class Teacher(Person):
    salary = models.DecimalField(
        default=6500.00,
        validators=[MinValueValidator(6500.00)],
        max_digits=10,
        decimal_places=2,
    )
    group = models.ManyToManyField(to="groups.Group")

    @classmethod
    def generate_instances(cls, count):
        faker = Faker()

        for _ in range(count):
            cls.objects.create(
                first_name=faker.first_name(),
                last_name=faker.last_name(),
                email=faker.email(),
                birth_date=faker.date_time_between(start_date="-80y", end_date="-25y"),
                group=random.choices(Group.objects.all()),
                salary=faker.random.randint(6500, 50000),
            )

    class Meta:
        ordering = ("first_name", "last_name")
