from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Q
from django.urls import reverse_lazy
from django.views.generic import CreateView, UpdateView, DeleteView, TemplateView

from teachers.forms import TeacherForm
from teachers.models import Teacher


class TeacherView(LoginRequiredMixin, TemplateView):
    model = Teacher
    template_name = "teachers/teachers.html"

    def get(self, request, *args, **kwargs):
        search_query = request.GET.get("q")
        if search_query:
            teachers = Teacher.objects.filter(
                Q(first_name__contains=search_query) | Q(last_name__contains=search_query)
            )
        else:
            teachers = Teacher.objects.all()
        self.extra_context = {"teachers": teachers}

        return self.render_to_response(self.extra_context)


class CreateTeacherView(LoginRequiredMixin, CreateView):
    model = Teacher
    form_class = TeacherForm
    template_name = "teachers/teachers_create.html"
    success_url = reverse_lazy("teachers:teachers")


class UpdateTeacherView(LoginRequiredMixin, UpdateView):
    model = Teacher
    form_class = TeacherForm
    template_name = "teachers/teachers_update.html"
    success_url = reverse_lazy("teachers:teachers")


class DeleteTeacherView(LoginRequiredMixin, DeleteView):
    model = Teacher
    success_url = reverse_lazy("teachers:teachers")
    template_name = "teachers/teachers_delete.html"
